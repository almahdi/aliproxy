AliProxy
====================

HTTP Proxy to load balance two or more internet connections. This was written due to the need to use two ISP connections on my MBP to accelerate the downloads.

### Copyrights:
> Copyright (c) 2013 Ali Almahdi

> Copyright (c) Twisted Matrix Laboratories.

> Licensed based on MIT License

> URL: http://opensource.org/licenses/mit-license.php

Original code was LoggingProxy Example from Twisted Examples and Proxy.py from twisted framework.
Modified  to achieve the required connection load balancing.

The below code is functional, however its not ideal. IPManager class uses round robin for queing the IPs.
Ideally, it shall be round robin with priority and destination management.

### Usage:
> python aliproxy.py

Set the download manager / browser to http proxy: localhost and port 8080

