#!/bin/env python
"""
# Copyrights:
# Copyright (c) 2013 Ali Almahdi
# Copyright (c) Twisted Matrix Laboratories.
# Licensed based on MIT License
# URL: http://opensource.org/licenses/mit-license.php
####
Original code was LoggingProxy Example from Twisted Examples and Proxy.py from twisted framework.
Modified  to achieve the required connection load balancing.

The below code is functional, however its not ideal. IPManager class uses round robin for queing the IPs.
Ideally, it shall be round robin with priority and destination management.
####

Usage:
python aliproxy.py

Set the download manager / browser to http proxy: localhost and port 8080
"""
try:
    from twisted.internet import reactor
    from twisted.web import proxy, http
    import urlparse
except:
    print "twisted framework is required."
    exit()

class AliProxyRequest(proxy.ProxyRequest):
    def process(self):
        """
        It's normal to see a blank HTTPS page. As the proxy only works
        with the HTTP protocol.
        """
        print "Request from %s for %s" % (
            self.getClientIP(), self.getAllHeaders()['host'])
        try:
            #  proxy.ProxyRequest.process(self)
            parsed = urlparse.urlparse(self.uri)
            protocol = parsed[0]
            host = parsed[1]
            port = self.ports[protocol]
            if ':' in host:
                host, port = host.split(':')
                port = int(port)
            rest = urlparse.urlunparse(('', '') + parsed[2:])
            if not rest:
                rest = rest + '/'
            class_ = self.protocols[protocol]
            headers = self.getAllHeaders().copy()
            if 'host' not in headers:
                headers['host'] = host
            self.content.seek(0, 0)
            s = self.content.read()
            clientFactory = class_(self.method, rest, self.clientproto, headers,
                                   s, self)

            self.reactor.connectTCP(host, port, clientFactory, bindAddress=(ip.getIP(), 0))
        
        except KeyError:
            print "HTTPS is not supported at the moment!"

class AliProxy(proxy.Proxy):
    requestFactory = AliProxyRequest

class AliProxyFactory(http.HTTPFactory):
    def buildProtocol(self, addr):
        return AliProxy()


class AliIPManager():
    def __init__(self):
        self.IPs = None
        self.used_ip = 0

    def __init__(self, ips):
        if(type(ips) != type([]) and len(ips) < 2):
            raise 'Invalid IP set.'

        self.IPs = ips
        self.used_ip = 0

    def setIPs(self,ips):
        self.__init__(ips)

    def getIP(self):
        if(self.used_ip == len(self.IPs)):
            self.used_ip = 0
            return self.getIP()

        self.used_ip = self.used_ip + 1
        return self.IPs[self.used_ip-1]





local_ips = ["127.0.0.1","10.10.10.1"]

ip = AliIPManager(local_ips)

reactor.listenTCP(8080, AliProxyFactory())
reactor.run()
